%Draws an ellipse
%
%Implicit equation:
%ax^2 + 9y^2 = 1
%
%where
%a = 174748/15000 - 7 = 4.6499
%

ezplot('4.6499*x.^2 + 9.*y.^2 - 1', [-1,1,-0.5,0.5])
axis equal
