%Draws an ellipse
%
%Implicit equation:
%x^2 / a + y^2 / 9 = 1
%

a = 174748 / 15000 - 7;
b = 9;

%parametrization
t = 0 : 0.001 : 2*pi;
x = sqrt(a) * cos( t );
y = sqrt(b) * sin( t );

plot( x, y )

hold

%line parametrization on 
%( sqrt(a/3), sqrt(6) )

x0 = sqrt( a / 3 );
y0 = sqrt( 6 );
t0 = asin( sqrt( 6 ) / 3 );

s = -4 : 0.1 : 4;

xr = x0 - s.*sqrt(a)*sin( t0 );
yr = y0 + s.*3*cos( t0 );

plot( xr, yr )
plot( x0, y0, '*r')
axis equal
