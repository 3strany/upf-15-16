%Draws a curve in R3

t = 0 : pi/100 : 2*pi;

x = 4.*cos( t ) + cos( 9.*t ) .* cos( t );
y = 4.*sin( t ) + cos( 9.*t ) .* sin( t );
z = sin( 9.*t );

plot3( x, y, z )
axis equal
