[x,y] = meshgrid(-10:0.05:10);
c = 174748 / 30000 - 3;
z = sin( c.*x ) .* sin( c.*y ) .* exp( - ( x.^2 + y.^2 ) / 36 );
figure; mesh( x, y, z )
[x,y] = meshgrid(-10:0.1:10);
z = sin( c.*x ) .* sin( c.*y ) .* exp( - ( x.^2 + y.^2 ) / 36 );
figure; surf( x, y, z )
figure; surf( x, y, z,'FaceColor','blue','EdgeColor','none' )
camlight left

k = sin( c*3 ) .* sin( c*3 ) .* exp( - 18 / 36 );

figure;contour( x, y, z, [k k] );
hold
plot(3,3,'*r')

%the component x,y of the gradient have the same number
grad = sin( 3*c ) .* exp( - 18 / 36 ) .* ( c * cos( 3*c ) - 3 * sin( 3*c ) / 18 );
s = -5 : 1 : 10;
plot( 3 + grad .* s, 3 + grad .* s );
axis equal
