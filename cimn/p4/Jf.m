function Jfx = Jf(x)
	
	J1 = 4*power(x(1),3) - 12*x(1)*power(x(2),2) - 1;
	J2 = 4*power(x(2),3) - 12*power(x(1),2)*x(2);
	J3 = 12*power(x(1),2)*x(2) - 4*power(x(2),3);
	J4 = 4*power(x(1),3) - 12*x(1)*power(x(2),2) - 1;
	
	Jfx = [J1,J2;J3,J4];

end
