function dibuixa (f, Jf, T, kmax)

cntr = [0,0];
radi = 2;
mida = 500;

esqu = cntr(1) - radi;
dalt = cntr(2) + radi;
pas  = 2 * radi / mida;
zero = zeros(5,2);

conques   = zeros(mida, mida);
velocitat = zeros(mida, mida);

trobats = 0;

for j = 1:mida
    for i = 1:mida
        [xk,k] = newton(f, Jf, [esqu + i * pas, dalt - j * pas], T, kmax);
        for n = 1:trobats
            if(norm([zero(n,1), zero(n,2)] - xk) < 2 * T)
                conques(i,j) = n / 5;
                break;
            end
        end
        if(conques(i,j) == 0)
            trobats = trobats + 1;
            zero(trobats,1) = xk(1);
            zero(trobats,2) = xk(2);
            conques(i,j) = trobats / 5;
        end
        velocitat(i,j) = k / kmax;
    end
end

imwrite(conques, 'conques.png');
imwrite(velocitat, 'velocitat.png');

end