function fx = f(x)

	c = (174748 - 125000) / 600000;
	
	x0 = power(x(1),4) + power(x(2),4) - 6*power(x(1),2)*power(x(2),2) - x(1) - 0.935 + c;
	y0 = 4*power(x(1),3)*x(2) - 4*x(1)*power(x(2),3) - x(2) - 1.475 - c;

	fx = [x0; y0];

end
