function [xk,k] = newton(f,Jf,x0,T,kmax)
%
% [xk,k] = newton(f,Jf,x0,T,kmax)
%
	xk = x0;
	
	for k = 1:kmax
		
		xk_1 = xk;
		
		xk = xk_1 - ( feval('Jf', xk_1) \ feval('f', xk_1))';
		
		if( norm( feval('f', xk) ) <= T)
			break;
		elseif( norm( xk - xk_1 ) / norm( xk ) <= T )
			break;
		end
	
	end
	
end
