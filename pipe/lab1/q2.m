%
%hist of x with
%b boxes
%
% q2( x, b )

function q2( x, b )
hist(x,b)
xlabel('x')
ylabel('counts')
title('Frequency histogram')
end
