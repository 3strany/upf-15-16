%
%Returns the mean of x
%
% mu = q3( x )
%
function [mu] = q3( x )

n = length(x);
sum = 0;

for i = 1:n

	sum = sum + x(i);

end

mu = sum/n;

end
