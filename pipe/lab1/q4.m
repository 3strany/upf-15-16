%
%Returns the standard deviation of x
%
% s = q4( x )
%

function s = q4( x )

n = length(x);
mu = q3(x);
sum = 0;

for i = 1:n

	sum = sum + (x(i) - mu )^2;

end

s = sqrt( sum/(n-1) );

end


