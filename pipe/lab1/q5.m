%
%Return the CI with alpha in x
%
% [a,b] = q(x, alpha)

function [a,b] = q5( x, alpha )

mu = q3(x);
s = q4(x);
n = length(x);

a = mu - s/sqrt(n)*norminv(1 - alpha/2);
b = mu + s/sqrt(n)*norminv(1 - alpha/2);

end
