%Returns number of samples
%in y that are in the CI
%with alpha of x
%
% num = q7( y, p_mean, alpha )
%
function [num] = q7( y, p_mean, alpha )

n = size(y,2);
num = 0;

for i = 1:n

	[a,b] = q5( y(:,i), alpha );

	if p_mean > a && p_mean < b
		num = num + 1;
	end

end

end
