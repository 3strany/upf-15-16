%
%Returns true/false if
%H0 rejected.
%
% reject_h0 = q8( x, h0, alpha )
%
function reject_h0 = q8( x, h0, alpha )

mu = q3(x);
s = q4(x);
n = length(x);

z_obs = ( mu - h0 ) / ( s / sqrt( n ) );

z_alpha = norminv( 1 - alpha );

if le(z_obs, -z_alpha)
	reject_h0 = true;
else
	reject_h0 = false;
end

end
