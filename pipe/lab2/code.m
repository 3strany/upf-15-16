D = csvread('3pa-pts-3pp.csv', 1, 0);
Y=D(:,1);% 3pa
X1=D(:,2);% pts
X2=D(:,3);% %3p
n=length(Y);
X=[ones(n,1), X1, X2];
beta=X\Y;
[mX1,mX2]=meshgrid(min(X1):(max(X1)-min(X1))/10:max(X1),min(X2):(max(X2)-min(X2))/10:max(X2));
figure(1);clf;
mesh(mX1,mX2,beta(1)+mX1*beta(2)+mX2*beta(3));
xlabel('total points')
ylabel('3p%')
zlabel('3PA')
hold on;
scatter3(X1,X2,Y,'r');
fprintf('Beta0 = %0.5f \n', beta(1) );
fprintf('Beta1 = %0.5f \n', beta(2) );
fprintf('Beta2 = %0.5f \n', beta(3) );

E = Y - X*beta;
figure(2);clf;
hist(E);
ylabel('counts')
xlabel('Error')

varE = var(E);

fprintf('\nError variance = %0.5f\n', varE)

YS = X*beta;

k = length(beta) - 1;

SSREG = (YS - mean(Y))'*(YS - mean(Y));
MSREG = SSREG / k;

SSERR = (Y - YS)'*(Y - YS);
MSERR = SSERR / (n - k - 1);

SSTOT = (Y - mean(Y))'*(Y - mean(Y));

R2 = SSREG / SSTOT;

fprintf('\nR2 = %0.5f\n', R2)

F_obs = MSREG / MSERR;

fprintf('\nF_obs = %0.5f\n', F_obs);

alpha = 0.05;

F_alpha = finv( 1 - alpha, k, n - k - 1 );

fprintf('F_alpha = %0.5f\n', F_alpha);

if( F_obs < F_alpha )
	fprintf('H0 accepted, f-test not passed\n')
else
	fprintf('H0 rejected, f-test passed\n')
end

%test for beta


for i = 2:length(beta)
	s2 = SSERR / ( n - k - 1 );
	cov_beta = inv( (X'*X) );
	cov_beta = cov_beta(i,i);
	s_beta = sqrt( s2 *  cov_beta);

	t_obs = beta(i) / s_beta;

	fprintf('\nt_obs_beta%d = %0.5f\n', i-1, t_obs)

	t_crit = -tinv( alpha/2, n - k - 1 );

	fprintf('acceptance interval beta%d =  [%0.5f, %0.5f]\n', i-1, -t_crit, t_crit )

	if( abs( t_obs ) >= t_crit )
		fprintf('H0 rejected, beta%d passes the test\n', i-1)
	else
		fprintf('H0 accepted, beta%d does not pass the test\n', i-1)
	end
end

for i = 2:length(beta)
	
	Sxx = (X(:,i) - mean( X(:,i) ))'*(X(:,i) - mean( X(:,i) ));

	interv = tinv(1 - alpha/2, n - 2) * sqrt(MSERR / Sxx);

	beta_min = beta(i) - interv;
	beta_max = beta(i) + interv;

	fprintf('\nconfidence interval beta%d: [%0.5f, %0.5f]', i-1, beta_min, beta_max)

end

fprintf('\n')
	




