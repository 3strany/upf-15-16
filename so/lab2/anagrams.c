#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

//---DEFINE----------------

#define MAXLENGTHWORD 50
#define MEMBLOCK 1000
//-------------------------

typedef struct anagram{

	char *wordA;
	char *wordB;

}anagram;


//---GLOBAL VARIABLES------

anagram *anagrams;

//-------------------------

//---FUNCTIONS-------------
int check_anagram(char a[], char b[]);

//-------------------------

int main( int argc, char *argv[] ){
	
	struct timeval start, end;
    long mtime, seconds, useconds;    
    gettimeofday(&start, NULL);
	
	char **dictionary = NULL;
	char buffer[MAXLENGTHWORD];
	FILE *usDic;

	anagrams = NULL;

	if( argc == 2 ){
		usDic = fopen( argv[1], "r" );
		if( usDic == NULL ){
			printf("Error reading file\n");
			exit(0);
		}
	}else{
		usDic = fopen( "p2-code/US.txt", "r" );
		if( usDic == NULL ){
			printf("Error reading file\n");
			exit(0);
		}
	}

	int size, wordlenght = 0, firstOfTheLength = 0, numberOfAnagrams = 0;
	for( size = 0; fgets( buffer, MAXLENGTHWORD, usDic ) != NULL; size++ ){
		
		if( size%MEMBLOCK == 0){
			dictionary = realloc( dictionary, sizeof( char* ) * ( size + MEMBLOCK ) );
		}
		buffer[strcspn(buffer, "\n")] = 0;
		dictionary[size] = malloc( strlen(buffer) );
		strcpy( dictionary[size], buffer );
		
		if( wordlenght != strlen( buffer ) ){
			
			wordlenght = strlen( buffer );
			firstOfTheLength = size;
			
		}else if( wordlenght > 4 ){
			int j;
			for( j = firstOfTheLength; j < size; j++)
				if( check_anagram( dictionary[j], dictionary[size] ) ){
					anagrams = realloc( anagrams, (numberOfAnagrams+1) * sizeof(anagram));
					anagrams[numberOfAnagrams].wordA = malloc( strlen( dictionary[j]));
					anagrams[numberOfAnagrams].wordB = malloc( strlen( dictionary[size]));
					strcpy( anagrams[numberOfAnagrams].wordA, dictionary[j] );
					strcpy( anagrams[numberOfAnagrams].wordB, dictionary[size] );
					numberOfAnagrams ++;
				}
		
		}
		
	}
	fclose( usDic );
	
	int i;
	fprintf( stdout, "COLLECTION OF ANAGRAMS\n");
	for( i = 0; i < numberOfAnagrams; i++ )	
		fprintf( stdout, "%s-%s are anagrms of %zu letters\n", anagrams[i].wordA, anagrams[i].wordB, strlen(anagrams[i].wordA) );
	
	//fprintf( stdout, "\npairs of anagrams: %d\n", numberOfAnagrams );
	
	for( i = 0; i < numberOfAnagrams; i++ ){
		free( anagrams[i].wordA );
		free( anagrams[i].wordB );
	}
	
	free( anagrams );
	
	for( i = 0; i < size; i++ )
		free( dictionary[i] );
	
	free( dictionary );
	
	gettimeofday(&end, NULL);

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

    fprintf( stdout, "\nElapsed time: %ld milliseconds\n", mtime);
	
}


int check_anagram(char a[], char b[]){

	int first[26] = {0}, second[26] = {0}, c = 0;
 
	while (a[c] != '\0') {
   		int i = a[c]-'a';
		if(i >= 0 && i < 26) first[i]++;
   		c++;
	}
 
	c = 0;
	while (b[c] != '\0') {
		int i = b[c]-'a';
		if(i >= 0 && i < 26) second[i]++;
		c++;
	}

	for (c = 0; c < 26; c++) {
		if (first[c] != second[c])
			return 0;
	}
	return 1;

}
