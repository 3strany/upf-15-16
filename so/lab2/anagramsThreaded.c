#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

//---DEFINE----------------

#define MAXLENGTHWORD 50
#define MEMBLOCK 1000
//-------------------------

typedef struct info{

	char **dict;
	int initialWord;
	int numberOfWords;

}info;

typedef struct anagram{

	char *wordA;
	char *wordB;

}anagram;


//---GLOBAL VARIABLES------

anagram *anagrams;
int numberOfAnagrams;
pthread_mutex_t lock;

//-------------------------

//---FUNCTIONS-------------
int check_anagram(char a[], char b[]);
void *thread_anagram( void * param );

//-------------------------

int main( int argc, char *argv[] ){
	
	struct timeval start, end;
    long mtime, seconds, useconds;    
    gettimeofday(&start, NULL);
	
	char **dictionary = NULL;
	char buffer[MAXLENGTHWORD];
	FILE *usDic;
	
	pthread_t tid[45];
	pthread_attr_t attr;
	info *threadinfo = NULL;

	anagrams = NULL;

	if( argc == 2 ){
		usDic = fopen( argv[1], "r" );
		if( usDic == NULL ){
			printf("Error reading file\n");
			exit(0);
		}else printf("File opened\n");
	}else{
		usDic = fopen( "p2-code/US.txt", "r" );
		if( usDic == NULL ){
			printf("Error reading file\n");
			exit(0);
		}else printf("File opened\n");
	}

	int size, wordlength = 0, firstOfTheLength = 0, numberOfThreads = 0;
	numberOfAnagrams = 0;
	for( size = 0; fgets( buffer, MAXLENGTHWORD, usDic ) != NULL; size++ ){

		if( size%MEMBLOCK == 0){
			dictionary = realloc( dictionary, sizeof( char* ) * ( size + MEMBLOCK ) );
		}
		buffer[strcspn(buffer, "\n")] = 0;
		dictionary[size] = malloc( strlen(buffer)+1 );
		strcpy( dictionary[size], buffer );
		
		if( wordlength != strlen( buffer )){
			if( strlen( buffer ) > 5 ){
				threadinfo = realloc( threadinfo, sizeof(info) * ( numberOfThreads + 1 ) );
				threadinfo[ numberOfThreads ].numberOfWords = size - firstOfTheLength;
				threadinfo[ numberOfThreads ].dict = dictionary;
				threadinfo[ numberOfThreads ].initialWord = firstOfTheLength;
			
				pthread_attr_init(&attr);
				printf("Create thread with: %d numberOfWords and %d firstOfTheLength\n",threadinfo[ numberOfThreads ].numberOfWords, threadinfo[ numberOfThreads ].initialWord );
				//tid = realloc( tid, (numberOfThreads+1) * sizeof(pthread_t) );
				pthread_create( &tid[numberOfThreads], &attr, thread_anagram, (void*)&threadinfo[ numberOfThreads ] );
			
				numberOfThreads++;
			}
			wordlength = strlen( buffer );
			firstOfTheLength = size;
			
		}
		
	}
	fclose( usDic );

	int i;
	printf("\nCOLLECTION OF ANAGRAMS\n");
	/*for( i = 0; i < numberOfAnagrams; i++ ){
	
		printf("%s <-> %s\n", anagrams[i], anagrams[i++] );
	
	}
	
	printf("\nsizes: %d %d\n", size, numberOfAnagrams/2 );
	
	for( i = 0; i < numberOfAnagrams; i++ )
		free( anagrams[i] );*/
	
	pthread_exit( NULL );
	
	free( anagrams );
	
	for( i = 0; i < size; i++ )
		free( dictionary[i] );
	
	free( dictionary );
	
	gettimeofday(&end, NULL);

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

    printf( "\nElapsed time: %ld milliseconds\n", mtime);
	
}


int check_anagram(char a[], char b[]){

	int first[26] = {0}, second[26] = {0}, c = 0;
 
	while (a[c] != '\0') {
   		int i = a[c]-'a';
		if(i >= 0 && i < 26) first[i]++;
   		c++;
	}
 
	c = 0;
	while (b[c] != '\0') {
		int i = b[c]-'a';
		if(i >= 0 && i < 26) second[i]++;
		c++;
	}

	for (c = 0; c < 26; c++) {
		if (first[c] != second[c])
			return 0;
	}
	return 1;

}


void *thread_anagram( void * param ){
	
	info *threadinfo = (info*) param;
	printf("Thread info: %s %d\n", threadinfo->dict[threadinfo->initialWord], threadinfo->numberOfWords );
	int i,j;
	for( i = threadinfo->initialWord; i < threadinfo->numberOfWords; i++)
		for( j = i + 1; j < threadinfo->numberOfWords; j++) {
				printf("i = %i j = %i\n", i, j);
				printf("%p %p\n", threadinfo->dict[i], threadinfo->dict[j]);
				printf("|%s |%s\n", threadinfo->dict[i], threadinfo->dict[j]);
				if( check_anagram( threadinfo->dict[i], threadinfo->dict[j] ) ){
					/*pthread_mutex_lock( &lock );
					anagrams = realloc( anagrams, (numberOfAnagrams+1) * sizeof(anagram));
					anagrams[numberOfAnagrams].wordA = malloc( strlen( threadinfo->dict[j]));
					anagrams[numberOfAnagrams].wordB = malloc( strlen( threadinfo->dict[i]));
					strcpy( anagrams[numberOfAnagrams].wordA, threadinfo->dict[j] );
					strcpy( anagrams[numberOfAnagrams].wordB, threadinfo->dict[i] );
					numberOfAnagrams ++;
					pthread_mutex_unlock( &lock );*/
				}
			}
	
	pthread_exit(0);

}
