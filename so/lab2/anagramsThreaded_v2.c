#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

//---DEFINE----------------

#define MAXLENGTHWORD 50
#define MEMBLOCK 1000
//-------------------------

typedef struct info{

	int initialWord;
	int numberOfWords;

}info;

typedef struct anagram{

	char *wordA;
	char *wordB;

}anagram;


//---GLOBAL VARIABLES------

anagram *anagrams;
int numberOfAnagrams;
pthread_mutex_t lock;
char **dictionary;

//-------------------------

//---FUNCTIONS-------------
int check_anagram(char a[], char b[]);
void *thread_anagram( void * param );

//-------------------------

int main( int argc, char *argv[] ){
	
	struct timeval start, end;
    long mtime, seconds, useconds;    
    gettimeofday(&start, NULL);
	
	dictionary = NULL;
	anagrams = NULL;
	char buffer[MAXLENGTHWORD];
	FILE *usDic;
	int i;
	
	pthread_t tid[45];
	info *threadinfo = NULL;



	if( argc == 2 ){
		usDic = fopen( argv[1], "r" );
		if( usDic == NULL ){
			fprintf( stdout, "Error reading file\n");
			exit(0);
		}
	}else{
		usDic = fopen( "p2-code/US.txt", "r" );
		if( usDic == NULL ){
			fprintf( stdout, "Error reading file\n");
			exit(0);
		}
	}

	int size, wordlength = 0, firstOfTheLength = 0, numberOfThreads = 0;
	numberOfAnagrams = 0;
	
	for( size = 0; fgets( buffer, MAXLENGTHWORD, usDic ) != NULL; size++ ){

		if( size%MEMBLOCK == 0){
			dictionary = realloc( dictionary, sizeof( char* ) * ( size + MEMBLOCK ) );
		}
		buffer[strcspn(buffer, "\n")] = 0;
		dictionary[size] = malloc( strlen(buffer)+1 );
		strcpy( dictionary[size], buffer );
		
		if( wordlength != strlen( buffer )){
			if( strlen( buffer ) > 4 ){
				threadinfo = realloc( threadinfo, sizeof(info) * ( numberOfThreads + 1 ) );
				threadinfo[numberOfThreads].initialWord = size;
				numberOfThreads++;
			}
			wordlength = strlen( buffer );
			firstOfTheLength = size;
			
		}

	}
	fclose( usDic );
	
	for( i = 0; i < numberOfThreads-1; i++ )
		threadinfo[i].numberOfWords = threadinfo[i+1].initialWord - threadinfo[i].initialWord;
		
	threadinfo[numberOfThreads-1].numberOfWords = size - threadinfo[numberOfThreads-1].initialWord;
	
	for( i = 0; i < numberOfThreads; i++ )
		pthread_create( &tid[i], NULL, thread_anagram, (void*)&threadinfo[ i ] );


	
	for( i = 0; i < numberOfThreads; i++ )
		pthread_join(tid[i], NULL);
	
	
	fprintf( stdout, "COLLECTION OF ANAGRAMS\n");
	for( i = 0; i < numberOfAnagrams; i++ )	
		fprintf( stdout, "%s-%s are anagrms of %zu letters\n", anagrams[i].wordA, anagrams[i].wordB, strlen(anagrams[i].wordA) );
	
	//fprintf( stdout, "\npairs of anagrams: %d\n", numberOfAnagrams );
	
	for( i = 0; i < numberOfAnagrams; i++ ){
		free( anagrams[i].wordA );
		free( anagrams[i].wordB );
	}
	
	free( anagrams );
	
	for( i = 0; i < size; i++ )
		free( dictionary[i] );
	
	free( dictionary );
	
	gettimeofday(&end, NULL);

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

    fprintf( stdout, "\nElapsed time: %ld milliseconds\n", mtime);
	
}


int check_anagram(char a[], char b[]){

	int first[26] = {0}, second[26] = {0}, c = 0;
 
	while (a[c] != '\0') {
   		int i = a[c]-'a';
		if(i >= 0 && i < 26) first[i]++;
   		c++;
	}
 
	c = 0;
	while (b[c] != '\0') {
		int i = b[c]-'a';
		if(i >= 0 && i < 26) second[i]++;
		c++;
	}

	for (c = 0; c < 26; c++) {
		if (first[c] != second[c])
			return 0;
	}
	return 1;

}


void *thread_anagram( void * param ){
	
	info *threadinfo = (info*) param;
	int i,j;
	for( i = threadinfo->initialWord; i < threadinfo->numberOfWords + threadinfo->initialWord; i++)
		for( j = i + 1; j < threadinfo->initialWord + threadinfo->numberOfWords; j++) {
			if( check_anagram( dictionary[i], dictionary[j] ) ){
				pthread_mutex_lock( &lock );
				anagrams = realloc( anagrams, (numberOfAnagrams+1) * sizeof(anagram));
				anagrams[numberOfAnagrams].wordA = malloc( strlen( dictionary[j]) + 1);
				anagrams[numberOfAnagrams].wordB = malloc( strlen( dictionary[i]) + 1);
				strcpy( anagrams[numberOfAnagrams].wordA, dictionary[j] );
				strcpy( anagrams[numberOfAnagrams].wordB, dictionary[i] );
				numberOfAnagrams ++;
				pthread_mutex_unlock( &lock );
			}
		}
	
	pthread_exit(0);

}
