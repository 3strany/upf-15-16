/*
	compilar con:
			gcc anagrams.c -o anagrams -pthread
*/
//includes

pthread_mutex_t lock;

void trhead_code( void * param ){
	
	int id = (int) *param;

	//...

	pthread_mutex_lock( &lock );

	//Seccion critica
	
	pthread_mutex_unlock( &lock );

}

int main(){

	pthread_mutex_init( &lock, NULL );

	pthread_t tid[100];
	
	int i;
	for( i = 0; i < 100; i++ )
		pthread_create( &tid[i], NULL, thread_code, &i );
	for( i = 0; i < 100; i++)
		pthread_join( &tid[i], NULL );

	pthread_mutex_destroy( &lock );

}
