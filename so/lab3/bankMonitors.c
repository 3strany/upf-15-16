#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

typedef struct sAccount {
	int id;
	double balance;
	bool bInUse;             // the account is being used?
	pthread_mutex_t mutex;   // lock to use/modify vars
	pthread_cond_t  freeAcc; // we wait in it when in Use
} Account;

#define N 5
#define NThreads 100
#define NTransfers 10000
Account bank[N];

bool withdraw(Account* acc, double amount) {
	if(acc->balance - amount >= 0)  {
		acc->balance -= amount;
		return true;
	}
	return false;
}
void deposit(Account* acc, double amount) {
	acc->balance += amount;
}

void transfer(Account* from, Account* to, double amount) {
	Account* acc1 = from;
	Account* acc2 = to;

	if( from > to ){

		pthread_mutex_lock( &acc2->mutex );
		while( acc2->bInUse )
			pthread_cond_wait( &acc2->freeAcc, &acc2->mutex );
		acc2->bInUse = true;
		pthread_mutex_lock( &acc1->mutex );
		while( acc1->bInUse )
			pthread_cond_wait( &acc1->freeAcc, &acc1->mutex );
		acc1->bInUse = true;

	}else{

		pthread_mutex_lock( &acc1->mutex );
		while( acc1->bInUse )
			pthread_cond_wait( &acc1->freeAcc, &acc1->mutex );
		acc1->bInUse = true;
		pthread_mutex_lock( &acc2->mutex );
		while( acc2->bInUse )
			pthread_cond_wait( &acc2->freeAcc, &acc2->mutex );
		acc2->bInUse = true;

	}


	bool bDone = withdraw(from,amount);
	if(bDone) deposit(to,amount);

	acc1->bInUse = false;
	pthread_cond_signal( &acc1->freeAcc );
	pthread_mutex_unlock( &acc1->mutex );
	acc2->bInUse = false;
	pthread_cond_signal( &acc2->freeAcc );
	pthread_mutex_unlock( &acc2->mutex );

}

void threadTransfer( void * param ){

	int i;
	for( i = 0; i < NTransfers; i++ ){

		double amount = rand()%50;
		int from = rand()%N;
		int to;
		do{ to = rand()%N; } while( from == to );

		transfer( &bank[from], &bank[to], amount );

	}

}


double bankInit() {
	double sum = 0;
	int i;
	for(i=0;i<N;i++) {
		bank[i].id = i;
		bank[i].balance = 100;
		bank[i].bInUse = false;
		sum += bank[i].balance;
		if (pthread_mutex_init(&bank[i].mutex, NULL) != 0) { printf("mutex error\n"); }
		if (pthread_cond_init(&bank[i].freeAcc, NULL) != 0) { printf("error initializing condition\n"); }
	}
	return sum;
}


int main(int argc, char *argv[])
{
	double sum = bankInit();
    printf("Initial bank capital: %f\n",sum);

	pthread_t* tid; /* the thread identifiers */
    tid = malloc(NThreads * sizeof(pthread_t));

	int i;
	for( i = 0; i < NThreads; i++)
		if( pthread_create( &tid[i], NULL, threadTransfer, NULL ) != 0) { printf("error creating thread\n"); }

	for( i = 0; i < NThreads; i++)
		if( pthread_join( tid[i], NULL ) != 0 ) { printf("error joinig thread\n"); }


    double sumEnd = 0;
	for(i=0;i<N;i++) {
		printf("Account %d balance : %f\n",i,bank[i].balance);
		sumEnd += bank[i].balance;
	}

    if(sumEnd != sum) printf("ERROR: ******** CORRUPT BANK!!!!!! *******\n");
    else printf("Final bank sum: %f\n",sum);

	return 0;
}
