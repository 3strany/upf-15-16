#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

typedef struct sAccount {
	int id;
	double balance;
	bool bInUse;             // the account is being used?
	pthread_mutex_t mutex;   // lock to use/modify vars
	pthread_cond_t  freeAcc; // we wait in it when in Use
} Account;

typedef struct sParamThread {
    int from;
    int to;
    float amount;
} ParamThread;

#define N 5
Account bank[N];

bool withdraw(Account* acc, double amount) {
	if(acc->balance - amount >= 0)  {
		acc->balance -= amount;
		return true;
	}
	return false;
}

void deposit(Account* acc, double amount) {
	acc->balance += amount;
}

bool transfer(Account* from, Account* to, double amount) {
	Account* acc1 = from;
	Account* acc2 = to;

	if( from > to ){

		pthread_mutex_lock( &acc2->mutex );
		while( acc2->bInUse )
			pthread_cond_wait( &acc2->freeAcc, &acc2->mutex );
		acc2->bInUse = true;
		pthread_mutex_lock( &acc1->mutex );
		while( acc1->bInUse )
			pthread_cond_wait( &acc1->freeAcc, &acc1->mutex );
		acc1->bInUse = true;

	}else{

		pthread_mutex_lock( &acc1->mutex );
		while( acc1->bInUse )
			pthread_cond_wait( &acc1->freeAcc, &acc1->mutex );
		acc1->bInUse = true;
		pthread_mutex_lock( &acc2->mutex );
		while( acc2->bInUse )
			pthread_cond_wait( &acc2->freeAcc, &acc2->mutex );
		acc2->bInUse = true;

	}


	bool bDone = withdraw(from,amount);
	if(bDone) deposit(to,amount);

	acc1->bInUse = false;
	pthread_cond_signal( &acc1->freeAcc );
	pthread_mutex_unlock( &acc1->mutex );
	acc2->bInUse = false;
	pthread_cond_signal( &acc2->freeAcc );
	pthread_mutex_unlock( &acc2->mutex );

    if(bDone) return true;
    else return false;

}

void * threadTransfer( void * param ){

    ParamThread *paramThread = (ParamThread*)param;

	if( transfer( &bank[paramThread->from], &bank[paramThread->to], paramThread->amount ) )
        return (void*) true;
    else return (void*) false;

}


void bankInit() {

	int i;
	for(i=0;i<N;i++) {
		bank[i].id = i;
		bank[i].balance = 100;
		bank[i].bInUse = false;
		if (pthread_mutex_init(&bank[i].mutex, NULL) != 0) { printf("mutex error\n"); }
		if (pthread_cond_init(&bank[i].freeAcc, NULL) != 0) { printf("error initializing condition\n"); }
	}
}

void printBalance(){
    int i;
    printf("\nBALANCE\n");
    for( i = 0; i < N; i++ )
        printf("Account %d balance : %02.2f\n", bank[i].id, bank[i].balance );
}


int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     bankInit();

     if (argc < 2) { fprintf(stderr,"ERROR, no port provided\n"); exit(1); }

     sockfd = socket(AF_INET, SOCK_STREAM, 0); // socket system call interface

     if (sockfd < 0) error("ERROR opening socket");

     bzero((char *) &serv_addr, sizeof(serv_addr));

     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);

     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) error("ERROR on binding");

     listen(sockfd,5);  // socket file descriptor and number of connections that can be awaiting

     clilen = sizeof(cli_addr);

     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr,  &clilen); // blocks until client connects

     if (newsockfd < 0)  error("ERROR on accept");

     bzero(buffer,256);

     n = read(newsockfd,buffer,255);
     if (n < 0) error("ERROR reading from socket");
     //printf("Here is the message: %s\n",buffer);
     printBalance();
     ParamThread paramThread;
     sscanf(buffer, "T%02d%02d%f", &paramThread.from, &paramThread.to, &paramThread.amount );
     printf("\nTransfer from account %d to account %d: amount %02.2f\n", paramThread.from, paramThread.to, paramThread.amount );

     pthread_t tid;
     if( pthread_create( &tid, NULL, threadTransfer, (void*) &paramThread ) != 0 ) { printf("error creating thread\n"); }

     bool bDone;

     if( pthread_join( tid, (void**) &bDone ) != 0 ) { printf("error joining thread\n"); }

     printBalance();
     bzero(buffer,256);
     if(bDone) sprintf(buffer,"Transfer completed");
     else sprintf(buffer,"Unable to transfer, not enough money in the account");
     n = write(newsockfd,buffer,strlen(buffer)+1);
     if (n < 0) error("ERROR writing to socket");

     close(newsockfd);
     close(sockfd);

     return 0;
}
