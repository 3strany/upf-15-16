#include <unistd.h>     // unix-like system calls read and write
#include <fcntl.h>      // unix-like file handling : open

#include <stdio.h>      // standard C lib input output basic functions compatible with Windows
#include <string.h>    // also from standard C lib : basic string functions like strlen
#include <stdlib.h>

#include <sys/time.h>
#include <time.h>

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>


#define MAXLENGTHBUFFER 100


int readSplit( int fin, char* buff, char s, int maxlen ) ;


int main( int argc, char* argv[] ){
	
	struct timeval start, end;
    long mtime, seconds, useconds;    
    gettimeofday(&start, NULL);
	
	char buffin[100];
    char buffout[100];
    int res = 0;
    
    int fin  = open( argv[1], O_RDONLY, S_IRUSR );
    if(fin < 0) fin = 0;
    
    while(res != -1) {
    	
		sprintf(buffout,"> ");
	    write(1,buffout,strlen(buffout));
        res = readSplit(fin, buffin, '\n', 100);
        
        if(res != -1) {
            
            char* pbin;
            char* pbinargs[3];
            int i;
            pbin = strtok( buffin, " " );
            
            for( i = 0; pbin != NULL; i++ ){
            	pbinargs[i] = pbin;
            	pbin = strtok( NULL, " " );
            	write( 1, pbinargs[i], strlen( pbinargs[i] ) );
            }
            
            write( 1, "\n", strlen("\n") );
			
            int pid;
            if( strncmp( pbinargs[0], "ls", 2 ) == 0 ){

            	pid = fork();
            	if( pid == 0 ){
            		execlp("ls", "ls", NULL);
            		write(1, "Error: Could not execute funciton\n", strlen("Error: Could not execute funciton\n"));
            		_exit(0);
            	}
            	
            	if( strncmp( pbinargs[i-1], "&", 1 ) != 0 )
            		waitpid( -1, NULL, 0 );
            	
            }else if( strncmp( pbinargs[0], "sleep", 5 ) == 0 ){
            	
            	pid = fork();
            	if( pid == 0 ){
            		
            		int sleepseconds = atoi(pbinargs[1]) * 1000000;
            		usleep( sleepseconds );
            		_exit(0);
            	}
            	
            	if( strncmp( pbinargs[i-1], "&", 1 ) != 0 )
            		waitpid( -1, NULL, 0 );
 
            }else if( strncmp( pbinargs[0], "backup", 6 ) == 0 ){
            
            	//do > backup c
				char cwd[256]; //variable to store current working directory
				memset( cwd, '\0', sizeof( cwd ) );
				getcwd( cwd, sizeof( cwd ) );
				strcat( cwd, "/" );
	
				//getting h:m:s on string time
				struct timeval tv;
				char time[17];
				time_t curtime;
				gettimeofday( &tv, NULL );
				curtime = tv.tv_sec;
				strftime( time,17,"%T/",localtime(&curtime));
				
				
				char newdir[256];//variable to store the new directory
				memset( newdir, '\0', sizeof( newdir ) );
				strcpy( newdir, cwd );
				strcat( newdir, time );

				DIR* dp;
				struct dirent *dptr = NULL;
			
				mkdir( newdir, S_IRWXU|S_IRWXG|S_IRWXO);
			
				dp = opendir( cwd );
				int files = 0;
				char filetype[5];
				memset( filetype, '\0', sizeof( filetype ) );
				filetype[0] = '.';
				strcat( filetype, pbinargs[1] );
				
				int pid;
				while(NULL != (dptr = readdir(dp)) ){
		            
					if( strstr( dptr->d_name, filetype ) != NULL ){
						pid = fork();
						if( pid == 0 ){
							execlp( "cp", "cp", dptr->d_name, newdir, NULL );
							printf("\nError cp\n");
							_exit(0);
						}
					}
					
    		    }
				
				if( strncmp( pbinargs[i-1], "&", 1 ) != 0 )
            		waitpid( -1, NULL, 0 );
  
            }

        }
    }
    
    while( waitpid( -1, NULL, 0 ) > 0 );
    
    gettimeofday(&end, NULL);

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

    sprintf( buffout, "Elapsed time: %ld milliseconds\n", mtime);
    write( 1, buffout, strlen( buffout ) );
    close( fin );
    return 0;
}

int readSplit( int fin, char* buff, char s, int maxlen ) {
    int i = 0;
    int rlen = 1;
    char c = 'a';
    while(c != s && rlen == 1 && i < maxlen) {
        rlen = read( fin, &c, 1);
        if(c != s && rlen == 1) {
            buff[i] = c;
            i++;
        }
    }
    if(i < maxlen) {
        buff[i] = '\0';
        if(rlen == 1) return i;
        else return -1;
    } else return -1;
}











